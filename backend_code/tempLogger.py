from flask import Flask, request, Response
from flask_restful import Resource, Api
from time import time

app = Flask(__name__)
api = Api(app)

temperatures = {}

class RegisterTemp(Resource):
    # def get(self):
    #     return temperatures

    def put(self, sensor, temp):
        temperatures[int(time())] = {"sensor" : int(sensor) , "temp" : float(temp) }
        with open("temperatures.csv", "a") as f:
            f.write("{},{},{}\n".format(int(time()), sensor, temp))
        return "Thank you!"

class GetTemp(Resource):
  def get(self):
    data = ""
    with open("temperatures.csv", 'r') as f:
        data = f.read()
    return Response(data, mimetype='text/xml')

class GetPlot(Resource):
  def get(self):
    data = []
    with open("temperatures.csv", 'r') as f:
        data = [l.split(',') for l in f.readlines()]
        data = [{ 'time' : int(x[0]), 'sensor' : int(x[1]), 'temp' : float(x[2]) } for x in data ]


api.add_resource(RegisterTemp, '/register/<int:sensor>/<float:temp>')
api.add_resource(GetTemp, '/temperatures.csv')
api.add_resource(GetPlot, '/plot')


if __name__ == '__main__':
    app.run(debug=True)

